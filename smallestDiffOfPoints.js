const ReadingFileAndGettingData = require('./readingFileAndGettingData.js');

class MinDiffOfPoints extends ReadingFileAndGettingData {
	constructor(fileName, col1, col2, nameCol) {
		super(fileName, col1, col2, nameCol); // Now 'this' is initialized by calling the parent constructor.
	}
}
module.exports = MinDiffOfPoints;
