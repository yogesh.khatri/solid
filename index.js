const readFIle = require('./readingFileAndGettingData.js');
const MinSpreadOfTemp = require('./minSpreadOfTemp.js');
const SmallestDiffOfPoints = require('./smallestDiffOfPoints');

const minTemp = new MinSpreadOfTemp('./weather.dat', 1, 2, 0);
const minPointDiff = new SmallestDiffOfPoints('./football.dat', 6, 8, 1);

minTemp.readingAndParsing('./weather.dat');
minPointDiff.readingAndParsing('./football.dat');
