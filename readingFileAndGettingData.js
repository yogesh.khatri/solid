// importing modules to read and parse file
const fs = require('fs');
const csv = require('fast-csv');

// parent class with common functions
class ReadingFileAndGettingData {
	// creating class variable to store rows, formated list of data and min variable of file
	constructor(fileName, col1, col2, nameCol) {
		this.fileName = fileName;
		this.col1 = col1;
		this.col2 = col2;
		this.nameCol = nameCol;
		this.allRowsOfFile = [];
		this.formatedListOfData = [];
	}

	//function to read file and get data in allRowsOfFile variable
	readingAndParsing(fileName) {
		// parsing rows and store data in allRowsOfFile
		fs
			.createReadStream(this.fileName)
			.pipe(csv.parse({ headers: false }))
			.on('data', row => this.creatingListOfRows(row))
			.on('end', row => this.formatingRowsForIteration(this.allRowsOfFile));
	}

	// function to create a list of rows, dependent on readingAndParsing function
	creatingListOfRows(row) {
		if (row[0] !== undefined && row[0].split(' ').length !== 0) {
			this.allRowsOfFile.push(row[0]);
		}
	}
	// function to create list of list  containing data
	formatingRowsForIteration(allRowsOfFile) {
		this.allRowsOfFile.shift();
		for (let i = 0; i < this.allRowsOfFile.length; i++) {
			var temp = this.allRowsOfFile[i].split(' ');
			this.formatedListOfData.push(temp.filter(element => element !== ''));
		}
		this.minDiff(this.formatedListOfData, this.col1, this.col2, this.nameCol);
	}
	// function to calculate min diff and print value
	minDiff(formatedListOfData, col1, col2, nameCol) {
		var min = this.formatedListOfData[0][this.col1] - this.formatedListOfData[0][this.col2];
		var team = this.formatedListOfData[0][this.nameCol];

		for (let i = 1; i < this.formatedListOfData.length; i++) {
			var col11 = this.formatedListOfData[i][this.col1];
			var col22 = this.formatedListOfData[i][this.col2];
			var diff = Math.abs(col11 - col22);
			if (min > diff) {
				min = diff;
				team = this.formatedListOfData[i][this.nameCol];
			}
		}
		console.log(team + ' : ' + min);
	}
}
module.exports = ReadingFileAndGettingData;
